---
layout: default
title: SpaceBotWar
---

SpaceBotWar
===========

SpaceBotWar is a vanity project. If it ever gets completed then it will
be an epic game, if not it will teach me a lot about the best use of
technologies such as HTML5, Web Sockets, Docker, front end Javascript
frameworks and much more.

The concept for SpaceBotWar is quite simple, in a nutshell it is a space
based multi-user game where you build your empire, send out ships to
collect resources and attack other players or Artificial Intelligence
(AI) enemies. You will see the influence of several other online games
such as [Lacuna Expanse](http://lacunaexpanse.com).

HTML5 has now come of age where it can replace Flash based games (which are not 
supported by Apple) or game engines such as Unity (which is no longer supported
by Chrome). HTML5 is supported by most modern browsers and the one's which
don't support it are not worth bothering with.

What SpaceBotWar offers, that very few games (if any) do, is the ability
to control the game via scripts (rather than just manually through the browser).
This adds a whole new dimension to the game, in effect, the game can be
extended by user defined scripts so that it runs even when the player is not
present.

A major influence has undoubtedly been [Lacuna Expanse](http://lacunaexpanse.com)
which has an open API to the game. This resulted in many people writing
external scripts to automate all aspects of the game. My own involvement
in the development of [Lacuna Expanse](http://lacunaexpanse.com) has resulted
in knowledge of how to develop scalable multi-user games such as SpaceBotWar.

This documentation serves as a guide to the developers of the SpaceBotWar 
game, a guide to people who want to write their own scripts via the API, and
a blog showing my thoughts and ideas as the game develops.

I hope you stay and give me feedback as parts of the game are released over
the coming months.

Iain C Docherty
---------------
